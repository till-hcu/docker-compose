# Docker Compose #

Die Docker Compose referenziert die Repositorien von Bitbucket.
Ohne das Herunterladen der Sourcen werden mit 'docker-compose up' die drei Prototypen gebuildet

* Python Flask Backend
* Vue Frontend - Builder Tool
* Geoerver

Soll der Geoserver lokal gebuildet werden, muss die entsprechende Variable in der docker-compose.yml gesetzt werden:

```
x-common-env: &common-env
  GEOSERVER_LOCAL='true'
```

Für das Builden auf lokalen Sourcen werden die .git URLs an die lokalen Pfade angepasst.

Desweiteren muss der service "geoserver" wieder einkommentiert werden.

Für einen gewissen Zeitraum sollte der Test-GeoServer der Prototypisierungsphase genutzt werden können:

https://csl-lig.hcu-hamburg.de/geoserver/web/

In diesem Fall muss der Geoserver aus den Services der Compose entfernt werden.
